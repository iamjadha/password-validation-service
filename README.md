This is password-validation-service project using embedded Tomcat + Thymeleaf template engine
Author: Jasmine Bhambra
Last updated: Jan 4, 2019
Repository: https://bitbucket.org/iamjadha/password-validation-service/downloads/

Prerequisites:
Unzip the project on your local system.
Make sure you have Java 1.8 and Maven installed.

Run:
In order to run, open command prompt/cygwin. Run the command mvn spring-boot:run. Now go to localhost:8080/validate to access the service.

Instructions:

Write a password validation service, meant to be configurable via IoC (using dependency injection engine of your choice). The service is meant to check a text string for compliance to any number of password validation rules. The rules currently known are: 
 
 Must consist of a mixture of lowercase letters and numerical digits only, with at least one of each.  Must be between 5 and 12 characters in length.  Must not contain any sequence of characters immediately followed by the same sequence. 
  
Include all artifacts in a zip file and please let us know how many hours you spent on the programming problem. The project should include a build capability in one of the following tools, Eclipse, IntelliJ, ant, or maven. The project should be ready for insertion into a production system. Show tests for the service and any constituent classes involved in fulfillment of the service. Also, show how to access and use the service at runtime. 
 