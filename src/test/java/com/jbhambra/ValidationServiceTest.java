package com.jbhambra;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.jbhambra.rules.TestBuilder;
import com.jbhambra.service.ValidationService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ValidationServiceTest extends TestBuilder{

    @Autowired
    ValidationService service;

    @Test
    public void ValidationServiceAutowired() {   
        assertNotNull(service);
    }
    
    @Test
	public void passwordShouldNotBeAllLowercaseChars() {
		assertThat(service.validatePassword(ALL_LOWERCASES)).contains(LETTERS_AND_DIGITS_RULE);
	}
    	
	@Test
	public void passwordShouldNotContainAlphaNumericChars() {
		assertThat(service.validatePassword(ALPHA_NUMERIC)).contains(LETTERS_AND_DIGITS_RULE);
	}
	
	@Test
	public void passwordShouldNotBeAllDigitsChars() {
		assertThat(service.validatePassword(ALL_DIGITS)).contains(LETTERS_AND_DIGITS_RULE);
	}
	
	@Test
	public void passwordShouldContainAtleastOneLowercaseAndOneDigitChar() {
		assertThat(service.validatePassword(LOWERCASES_AND_DIGITS)).isNullOrEmpty();
	}
	
	@Test
	public void passwordShouldNotContainAnyUppercaseChars() {
		assertThat(service.validatePassword(LOWERCASES_UPPERCASES_AND_DIGITS)).contains(LETTERS_AND_DIGITS_RULE);
	}
	
	@Test
	public void passwordShouldNotBeLessThanFiveChars() {
		assertThat(service.validatePassword(FOUR_LETTERS)).contains(LENGTH_RULE);
	}

	@Test
	public void passwordShouldtBeFiveChars() {
		assertThat(service.validatePassword(FIVE_LETTERS)).isNullOrEmpty();
	}

	@Test
	public void passwordShouldBeTwelveChars() {
		assertThat(service.validatePassword(TWELVE_LETTERS)).isNullOrEmpty();
	}

	@Test
	public void passwordShouldNotBeMoreThanTwelveChars() {
		assertThat(service.validatePassword("1234567891123")).contains(LENGTH_RULE);
	}
	
}