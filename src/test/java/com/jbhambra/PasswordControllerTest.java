package com.jbhambra;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.jbhambra.controller.PasswordController;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PasswordControllerTest {

	@Autowired
	private PasswordController controller;
	
	@Test
	public void contextLoads() {
		assertThat(controller).isNotNull();
	}
	

}

