package com.jbhambra.rules;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class TestSequenceImmediatelyFollowedByChar extends TestBuilder {

	PasswordMustNotContainAnySequenceOfCharsImmediatelyFollowedBySameSequence rule = new PasswordMustNotContainAnySequenceOfCharsImmediatelyFollowedBySameSequence();
	
	@Test
	public void passwordWithNonRepeatSequenceAllowed() {
		assertThat(rule.check(NON_REPEAT_SEQUENCE)).isTrue();
	}
	
	@Test
	public void passwordWithRepeatSequenceNotAllowed() {
		assertThat(rule.check(REPEAT_SEQUENCE)).isFalse();
	}

}
