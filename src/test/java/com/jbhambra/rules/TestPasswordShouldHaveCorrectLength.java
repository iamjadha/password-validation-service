package com.jbhambra.rules;

import static org.assertj.core.api.Assertions.*;

import org.junit.Test;

public class TestPasswordShouldHaveCorrectLength extends TestBuilder {

	PasswordBetweenFiveToTwelveChars rule = new PasswordBetweenFiveToTwelveChars();

	@Test
	public void passwordShouldNotBeLessThanFiveChars() {
		assertThat(rule.check(FOUR_LETTERS)).isFalse();
	}

	@Test
	public void passwordShouldtBeFiveChars() {
		assertThat(rule.check(FIVE_LETTERS)).isTrue();	
	}

	@Test
	public void passwordShouldBeTweleveChars() {
		assertThat(rule.check(TWELVE_LETTERS)).isTrue();
	}

	@Test
	public void passwordShouldNotBeMoreThanTweleveChars() {
		assertThat(rule.check(THIRTEEN_LETTERS)).isFalse();
	}
}
