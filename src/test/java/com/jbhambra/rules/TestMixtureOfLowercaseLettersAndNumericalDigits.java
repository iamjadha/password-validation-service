package com.jbhambra.rules;

import com.jbhambra.rules.PasswordMixLowercaseAndNumericalDigits;

import org.junit.Before;
import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

public class TestMixtureOfLowercaseLettersAndNumericalDigits extends TestBuilder {

	PasswordMixLowercaseAndNumericalDigits rule = new PasswordMixLowercaseAndNumericalDigits();
	
	@Before
	public void setUp() throws Exception {
	}
	
	@Test
	public void passwordShouldNotBeAllLowercaseChars() {
		assertThat(rule.check(ALL_LOWERCASES)).isFalse();
	}
	
	@Test
	public void passwordShouldNotContainAlphaNumericChars() {
		assertThat(rule.check(ALPHA_NUMERIC)).isFalse();
	}
	
	@Test
	public void passwordShouldNotBeAllDigitsChars() {
		assertThat(rule.check(ALL_DIGITS)).isFalse();
	}
	
	@Test
	public void passwordShouldContainAleastOneLowercaseAndOneDigitChar() {
		assertThat(rule.check(LOWERCASES_AND_DIGITS)).isTrue();
	}
	
	@Test
	public void passwordShouldNotContainAnyUppercaseChars() {
		assertThat(rule.check(LOWERCASES_UPPERCASES_AND_DIGITS)).isFalse();
	}
	
}
