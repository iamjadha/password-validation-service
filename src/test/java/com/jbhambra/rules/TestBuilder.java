package com.jbhambra.rules;

public abstract class TestBuilder {
	
	protected static final String ALL_LOWERCASES  = "abcde";
	protected static final String ALL_DIGITS = "123456";
	protected static final String LOWERCASES_AND_DIGITS = "abcde2019";
	protected static final String LOWERCASES_UPPERCASES_AND_DIGITS = "Abcded2019";
	protected static final String ALPHA_NUMERIC = "apple!!!";
	protected static final String UPPERCASE_ANG_DIGITS = "APPLE123";
	
	protected static final String FOUR_LETTERS = "ab12";
	protected static final String FIVE_LETTERS = "ab123";
	protected static final String TWELVE_LETTERS = "abcde12345ab";
	protected static final String THIRTEEN_LETTERS = "abcde12345abc";
	
	protected static final String REPEAT_SEQUENCE = "giraffe";
	protected static final String NON_REPEAT_SEQUENCE = "abcdefg";

	protected static final String LENGTH_RULE = "Must be between 5 and 12 characters in length.";
	protected static final String LETTERS_AND_DIGITS_RULE = "Must consist of a mixture of lowercase letters and numerical digits only, with at least one of each.";
    protected static final String SEQUENCE_RULE = "Password must not contain any sequence of characters immediately followed by the same sequence.";


}
