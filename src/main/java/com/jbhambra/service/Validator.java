package com.jbhambra.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.jbhambra.rules.PasswordBetweenFiveToTwelveChars;
import com.jbhambra.rules.PasswordMixLowercaseAndNumericalDigits;
import com.jbhambra.rules.PasswordMustNotContainAnySequenceOfCharsImmediatelyFollowedBySameSequence;

@Service
public class Validator implements ValidationService {

	public Set<String> validatePassword(String password) {
		Set<String> failures = new HashSet<String>();
		
		checkLengthRule(password, failures);
		checkLetterAndDigitAndCaseRule(password, failures);
		checkSequenceRepetitionRule(password, failures);
		return failures;
	}

	private void checkSequenceRepetitionRule(String password, Set<String> failures) {
		PasswordMustNotContainAnySequenceOfCharsImmediatelyFollowedBySameSequence rule = new PasswordMustNotContainAnySequenceOfCharsImmediatelyFollowedBySameSequence();
		if(!rule.check(password)) {
			failures.add(rule.getErrorMessage());
		}
	}

	private void checkLetterAndDigitAndCaseRule(String password, Set<String> failures) {
		PasswordMixLowercaseAndNumericalDigits rule = new PasswordMixLowercaseAndNumericalDigits();
		if(!rule.check(password)) {
			failures.add(rule.getErrorMessage());
		}
	}

	private boolean checkLengthRule(String password, Set<String> failures) {
		PasswordBetweenFiveToTwelveChars rule = new PasswordBetweenFiveToTwelveChars();
		if(!rule.check(password)) {
			failures.add(rule.getErrorMessage());
			return false;
		}
		return true;
		
	}

}
