package com.jbhambra.service;

import java.util.Set;

import org.springframework.stereotype.Component;

@Component
public interface ValidationService {

	public Set<String> validatePassword(String passwordText);

}
