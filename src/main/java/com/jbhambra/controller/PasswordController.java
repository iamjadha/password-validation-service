package com.jbhambra.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.jbhambra.domain.Password;
import com.jbhambra.service.ValidationService;

@Controller
public class PasswordController {
	
	@Autowired 
	private ValidationService service;
	
	@GetMapping("/validate")
	public String passwordForm(Model model) {
		model.addAttribute("password", new Password());
		model.addAttribute("messages", "");
		return "passwordPage";
	}

	@PostMapping(path = "/validate")
	public String computeResult(Password password, Model model) {
		model.addAttribute("messages", service.validatePassword(password.getContent()));
		return "resultPage";
	}
	

}
