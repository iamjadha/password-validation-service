package com.jbhambra.rules;

public abstract class ValidationRule {
	
	private String errorMessage;
	
	abstract public boolean check(String password);
	
	public String getErrorMessage() {
		return errorMessage;
	}
}
