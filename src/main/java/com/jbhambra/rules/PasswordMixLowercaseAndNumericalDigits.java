package com.jbhambra.rules;

public class PasswordMixLowercaseAndNumericalDigits extends ValidationRule {

	private String errorMessage;

	private boolean hasLower = false;

	private boolean hasUpper = false;
	
	public PasswordMixLowercaseAndNumericalDigits() {
		errorMessage = "Must consist of a mixture of lowercase letters and numerical digits only, with at least one of each.";
	}

	public boolean check(String password) {
		return (!checkUppercase(password) && checkAlphaNumeric(password) && checkLowercase(password)
				&& checkAtleastOneDigit(password));		
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public boolean checkLowercase(String password) {
		char[] characters = password.toCharArray();

		for (char c : characters) {
			if (Character.isLowerCase(c)) {
				hasLower = true;
			}
		}
		return hasLower;
	}

	public boolean checkUppercase(String password) {
		char[] characters = password.toCharArray();

		for (char c : characters) {
			if (Character.isUpperCase(c)) {
				hasUpper = true;
			}
		}
		return hasUpper;
	}

	public boolean checkAtleastOneDigit(String password) {
		boolean checkAtleastOneNumber = password.matches(".*\\d.*");
		return checkAtleastOneNumber;
	}

	public boolean checkAlphaNumeric(String password) {
		char[] characters = password.toCharArray();
		for (char c : characters) {
			if (!Character.isLetterOrDigit(c))
				return false;
		}
		return true;
	}

}
