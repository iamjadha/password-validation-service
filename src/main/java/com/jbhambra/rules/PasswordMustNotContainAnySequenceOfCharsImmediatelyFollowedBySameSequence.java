package com.jbhambra.rules;

public class PasswordMustNotContainAnySequenceOfCharsImmediatelyFollowedBySameSequence extends ValidationRule {

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public PasswordMustNotContainAnySequenceOfCharsImmediatelyFollowedBySameSequence() {
		errorMessage= "Must not contain any sequence of characters immediately followed by the same sequence.";
	}

	public boolean check(String password) {
		for (int index = 0; index < password.length(); index++) {
			char givenChar = password.charAt(index);
			int nextIndex = password.indexOf(givenChar, index + 1);
			
			while (nextIndex != -1) {
				String strBetweenCurrentIndexAndIndexOfFirstOccuranceOfGivenChar = password.substring(index, nextIndex);
	
				if((2 * nextIndex - index) <= password.length()) {
					String strstringBetweenOfFirstOccuranceOfGivenCharAndNextChar = password.substring(nextIndex, 2 * nextIndex - index);
					if (strBetweenCurrentIndexAndIndexOfFirstOccuranceOfGivenChar.equals(strstringBetweenOfFirstOccuranceOfGivenCharAndNextChar)) {
						return false;
					}
				}
				nextIndex = password.indexOf(givenChar, nextIndex + 1);
			}
			
		}
		return true;
	}
}
