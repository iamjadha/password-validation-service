package com.jbhambra.rules;

public class PasswordBetweenFiveToTwelveChars extends ValidationRule {

	private String errorMessage;

	public PasswordBetweenFiveToTwelveChars() {
		errorMessage = "Must be between 5 and 12 characters in length.";
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	@Override
	public boolean check(String password) {
		return (password.length() >= 5 && password.length() <= 12);
	}

}
